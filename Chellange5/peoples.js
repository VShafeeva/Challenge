// Глобальные переменные
const startPeopleCount = 5;
let peoples = {};
const btnAddPeople = document.getElementById('btnAdd');
let div = document.getElementsByClassName('peoples-container')[0];


// Функция генерации цвета
function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let randomColor = '#';

    for (let i = 0; i < 6; i++) {
        randomColor += letters[Math.floor(Math.random() * 16)];
    }

    return randomColor;
}

// Функция генерации имения
function generateName() {
    let name = "";
    let possible = "01";

    for (let i = 0; i < 5; i++)
        name += possible.charAt(Math.floor(Math.random() * possible.length));

    return name;
}

// Создание массива человечков и сохранение их в глобальной переменной
createPeoples = (peopleCount) => {
    for (let i = 1; i <= peopleCount; i++) {
        peoples[i] = {
            id: i,
            name: generateName(),
            color: getRandomColor()
        }
    }
};

// Заселение человечков на страницу и сохранение их DOM элемента в переменной


//добавление человека в Объект
addToObject = () => {
    peoples[Object.keys(peoples).length + 1] = {
        id: Object.keys(peoples).length + 1,
        name: generateName(),
        color: getRandomColor()
    }
};
// добавление нового тега "i" cо значениями нового человека в объекте
addTag = () => {
    let addn = document.createElement('i');
    let wrapper = document.getElementsByClassName('peoples-container')[0];
    let ih = peoples[Object.keys( peoples ).length];
    addn.className = 'fas fa-male';
    addn.setAttribute('data-id', ih.id);
    addn.setAttribute('data-name', ih.name);
    addn.setAttribute('style', 'color: ' + ih.color);
    wrapper.appendChild(addn);
};
//

// Изменение цвета человекчка
changePeopleColor = (event) => {
    let newColor = getRandomColor();
        event = window.event;
        let t = event.target;
        t.style.color = newColor;
    };


getDomPeopleById = (peopleId) => {
    return peoples[peopleId].domElement;
};




// Функция, которая срабатывает при загрузке структуры документа (когда доступны
// DOM элементы
domContentLoaded = () => {
    createPeoples(startPeopleCount);
    displayPeoples(peoples);
};
// TODO: возможно стоит поменять название на более подходящее
displayPeoples = (peoplesForDisplay) => {
    let wrapperPeoples = document.getElementsByClassName('peoples-container')[0];

    for (let people in peoplesForDisplay) {
        let peopleElement = document.createElement('i');

        peopleElement.className = 'fas fa-male';
        peopleElement.setAttribute('data-id', peoplesForDisplay[people].id);
        peopleElement.setAttribute('data-name', peoplesForDisplay[people].name);
        peopleElement.setAttribute('style', 'color: ' + peoplesForDisplay[people].color);

        wrapperPeoples.appendChild(peopleElement);

        peoples[people].domElement = peopleElement;
    }
};


//функция по нажатию на кнопку "+1 People"
btnAddPeople.onclick = function add() {
    addToObject();
    addTag();
}
// Функция, которая срабатывает при клике на человечка
div.addEventListener('click', changePeopleColor);

document.addEventListener('DOMContentLoaded', domContentLoaded);