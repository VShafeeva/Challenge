
let result = 0;
let resultCalc = document.getElementById("result");
let buttonPlus = document.getElementById("plus");
let buttonMinus = document.getElementById("minus");

buttonPlus.addEventListener('click', plusValue);
buttonMinus.addEventListener('click',minusValue);

function plusValue() {
    result++;
    if (result >  10 ){
       result = 0;
    }
    resultCalc.value = result;
}

function minusValue(){
    if (result > 0) {
        result--;
        resultCalc.value = result;
    }
}

function keyPress(e) {
    switch(e.keyCode) {

        case 37:  // если нажата клавиша влево
            plusValue();
            break;

        case 39:   // если нажата клавиша вправо
            minusValue();
            break;
    }
}
addEventListener('keydown', keyPress);

let buttonNext = document.getElementById("next");

buttonNext.onclick = function test() {
    let element = document.getElementById("but");
    let inp = document.createElement ("button");
    let inp2 = document.createElement("button");
    let res = document.createElement("input");

        inp.innerHTML = "Увеличить +1";
        inp.style.marginLeft = "35px";
        inp.style.marginTop = "20px";
        inp2.innerHTML = "Уменьшить -1";
        res.innerHTML = " 0 ";
        res.style.marginRight = "30px";

   element.appendChild(inp);
   element.appendChild(inp2);
   element.appendChild(res);
}

