
const buttonMiddle = document.getElementById("resultMediana");
const medianaNumberDisplay = document.getElementById("result");

//получение массива из инпута и разбивка его по разделителю " , "
function getNumbers() {
    let intArray =[];
    let massNumber = document.getElementById("numbers").value;
    let strArray = massNumber.split(',');

    //преобразование строчного массива в числовой
    let leng = strArray.length;

    for ( let i = 0; i < leng; i++) {
        intArray[i] = parseInt(strArray[i], 10);
    }

     return intArray;
}


//сортировка пузырьком
function sortArray(intArray) {
    let count = intArray.length-1;

     for (let i = 0; i < count; i++) {
        for (let j = 0; j < count - i; j++) {
            if (intArray[j] > intArray[j + 1]) {
                let max = intArray[j];
                intArray[j] = intArray[j + 1];
                intArray[j + 1] = max;
            }
        }
    }

    return intArray;
}

//поиск медианы
function medianaSerch(intArray, mediana) {
    let leng = intArray.length;

    //проверка массива на четность кол-ва элементов
    if (leng % 2 === 0 ) {
        let  massElementOne = intArray[leng / 2];
        let  massElementTwo = intArray[leng / 2 - 1];
        mediana = (+massElementOne + +massElementTwo) / 2;
    }
    else {
         mediana = intArray[leng / 2 - 0.5];
    }

    return mediana;
}

//вызов функций на нажатии кнопки
buttonMiddle.onclick = function () {
    medianaNumberDisplay.innerHTML = medianaSerch(sortArray(getNumbers()));
}
